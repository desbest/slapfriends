<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="screwdefaultbuttons.js" ></script>
<script type="text/javascript" src="validation/jquery.validate.min.js" ></script>

<?php require("config-lite.php"); ?>
<script type="text/javascript">

function sendRequestToManyRecipients() {
    FB.ui({method: 'apprequests',
        message: 'I\'ve just slapped you.'
    }, requestCallback);
}
    
function requestCallback(response) {
    // Handle callback here
    if (response){
        for(var i = 0; i < response.to.length; i++) {
            //alert(\"Invitation was sent to: \" + response.to[i]);
            
            /////////////////////////
            ////Step 1: Fetch the Request ID
            var initiator =("<?php echo $user_profile['id'] ?>");
            var concludor = response.to[i];
            var chosencolour = $('input[name=colour]:checked').val()
            
            /////////////////////////
            ////Step 2: Create the match in db
            $.ajax({
                type: 'GET',
                url: 'ajaxcontrols.php?initiator='+ initiator +'&concludor=' +concludor +'&initiator_colour='+ chosencolour+'',
                /* data: dataString, */
                cache: false,
                success: function(html){ },
                //error: function(html){ alert('ajax FAILED!!!!!'); },
            });
            
            /////////////////////////
            ////Step 3: Add one point
            $.ajax({
                type: 'GET',
                url: 'ajaxcontrols.php?fbprofileid='+ initiator +'&morepoints=1&initiator='+ initiator +'&concludor=' +concludor +'&giveto=initiator',
                /* data: dataString, */
                cache: false,
                success: function(html){ },
                //error: function(html){ alert('ajax FAILED!!!!!'); },
            });
            ////
            $('.noticeHere').append('<div class=\'notice\'>Your match request been sent, and is awaiting a response!</p>');
                
        }
    }
}

$(document).ready(function(){
    $('input:radio').screwDefaultButtons({ 
      checked: "url(graphics/checked.png)",
      unchecked: "url(graphics/unchecked.png)",
      width: 40,
      height: 40
   });
   
   
   $("#myform").validate({
        rules: {
			colour: "required"
		},
		messages: {
            colour: {
                required: "Make sure you choose a colour."
            }
		},
        errorLabelContainer: "#errorshere",
        submitHandler: function() {
                var dataString = $("#myform").serialize();
                var formMode = $("#formMode").val();
                
                if (formMode == 'newmatch') { sendRequestToManyRecipients(); }
                if (formMode == 'resmatch') {  
                        var chosencolour = $('input[name=colour]:checked').val();
                        <?php
                        $winningMatch = mysql_query("SELECT * FROM matches WHERE concludor='$user_profile[id]' && concludor_colour='' ");
                        $concludingTurn = mysql_num_rows($winningMatch); $winningMatch = mysql_fetch_array($winningMatch); 
                        $correctColour = $winningMatch['initiator_colour']; $concludor = $winningMatch['concludor']; $initiator = $winningMatch['initiator'];
                        ?>
                        
                        ///////////////////////// (final step)
                        ////Step 2: Save win to database
                        $.ajax({
                            type: 'GET',
                            url: 'ajaxcontrols.php?guessedcolour='+ chosencolour +'&initiator=<?php echo $initiator ?>&concludor=<?php echo $concludor ?>',
                            // data: dataString,
                            cache: false,
                            success: function(html){
                            },
                            //error: function(html){ alert('ajax FAILED!!!!!'); },
                        });
                            
                        if (chosencolour == '<?php echo $correctColour ?>'){
                            /////////////////////////
                            ////Step 1: Add five points
                            $.ajax({
                                type: 'GET',
                                url: "ajaxcontrols.php?fbprofileid=<?php echo $concludor ?>&morepoints=5&initiator=<?php echo $initiator ?>&concludor=<?php echo $concludor ?>&giveto=concludor",
                                /* data: dataString, */
                                cache: false,
                                success: function(html){
                                },
                                //error: function(html){ alert('ajax FAILED!!!!!'); },
                            });
                            $('.noticeHere').append('<div class=\'notice\'>Your have won this match, gaining 5 points!</p>');
                            $("#guesscolour").fadeOut(3000);
                        } else {
                            //you get 0 points
                            /////////////////////////
                            ////Step 1: Post on wall saying powerslapped
                            //      ^^ check the ajax above

                            $('.noticeHere').append('<div class=\'notice\'>The match failed to have a winner. Unlucky.</p>');
                            $("#guesscolour").fadeOut(3000);
                        }
                }
        }

   })
});
</script>
</head>
<body>
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
    FB.init({ 
        appId:'<?php echo $config['appId']; ?>', cookie:true, 
        status:true, xfbml:true, oauth:true
    });
</script>

<div class="container">
<div id="response"></div>
<img src="graphics/header.png" width="743" height="81" />

<div class="subheader">

<?php
/////////////////////
///// the header
$fetchgame = mysql_query("SELECT * FROM matches WHERE concludor='$user' && concludor_colour='' ");
$countgames = mysql_num_rows($fetchgame);
if ($countgames == 0 ){
    $opponent = "noneyet";
    echo "<img src=\"graphics/header-initiator.png\" width=\"743\" height=\"119\" />";
} else {
    $game = mysql_fetch_array($fetchgame); $opponent = "initiator"; 
    echo "<img src=\"graphics/header-concludor.png\" width=\"743\" height=\"119\" />";
} 
echo "<div class=\"noticeHere\"></div>";
/////////////////////
///// initiator wait?
$fetchwaiting = mysql_query("SELECT * FROM matches WHERE initiator='$user' && concludor_colour='' ");
$countwaiting = mysql_num_rows($fetchwaiting);
if ($countwaiting >0){ $game = mysql_fetch_array($fetchwaiting); $opponent = "concludor"; }
/////////////////////
///// player avatars
    $mydpic = "https://graph.facebook.com/$user/picture?access_token=$access_token";
if ($countgames >0 || $countwaiting >0) {
    $opdpic = "https://graph.facebook.com/$game[$opponent]/picture?access_token=$config[appId]|$config[secret]";
    echo "
    <div class='playerzone'>
    <div class='player'><img src='$mydpic' />
        <div class='role'><span>Me</span></div></div>
    <div class='player'><img src='$opdpic' />
        <div class='role'><span>Opponent</span></div></div>
    </div>
    ";
} else {
    echo "
        <div class='playerzone'>
        <div class='player'><img src='$mydpic' />
            <div class='role'><span>Me</span></div></div>
        </div>
    ";
}
?>
</div>

<?php if ($user): ?>
    
    <div id="errorshere">
    </div> 
    <form id="myform" method="POST" action="">
    
    <div class="colourblock red">
        <input type="radio" class="bigcheck" name="colour" value="red" /></div>
    <div class="colourblock teal">
        <input type="radio" class="bigcheck" name="colour" value="teal" /></div>
    <div class="colourblock fuschia">
        <input type="radio" class="bigcheck" name="colour" value="fuschia" /></div>
    <div class="colourblock orange">
        <input type="radio" class="bigcheck" name="colour" value="orange" /></div>
    <div class="colourblock yellow">
        <input type="radio" class="bigcheck" name="colour" value="yellow" /></div>
    
    <div class="colourblock lime">
        <input type="radio" class="bigcheck" name="colour" value="lime" /></div>
    <div class="colourblock aquamarine">
        <input type="radio" class="bigcheck" name="colour" value="aquamarine" /></div>
    <div class="colourblock burgundy">
        <input type="radio" class="bigcheck" name="colour" value="burgundy" /></div>
    <div class="colourblock blue">
        <input type="radio" class="bigcheck" name="colour" value="blue" /></div>
    <div class="colourblock purple">
        <input type="radio" class="bigcheck" name="colour" value="purple" /></div>
        
    <div style="clear: both;"></div>
    
    <?php
    if ($opponent == "noneyet"){
    echo "<input type='submit' id='chosecolour' class='bigbutton' value='Slap your friends' 
            style='float: right; margin-right: 12px;' />
            <input type='hidden' id='formMode' name='formMode' value='newmatch' />
    </form> ";
    } elseif ($opponent == "initiator"){
    echo "<input type='submit' id='guesscolour' class='bigbutton' value='Guess the colour' 
            style='float: right; margin-right: 12px;' />
            <input type='hidden' id='formMode' name='formMode' value='resmatch' />
            <input type='hidden' id='initiator' name='initiator' value='$game[initiator]' />
    </form> ";    
    } elseif ($opponent == "concludor") {
        echo "<div class='footer'>You have to wait for your opponent to respond to your slap challenge.</div></form>";
    }
    ?>
    <!-- <fb:add-to-timeline></fb:add-to-timeline> -->

    <div class="footer">
    <a href="<?php echo $logoutUrl; ?>"><img src="graphics/fb-logout.png"/></a>
    </div>
<?php elseif(!$user): ?>
    To play this game, you have to be logged into Facebook.<br/>
    <a href="<?php echo $loginUrl; ?>"><img src="graphics/fb-login.png"/></a>
<?php endif; ?>


</div>
</body>
</html>