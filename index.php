<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
</head>
<body><?php require("config.php"); ?>
<div class="container">
<div id="response"></div>
<img src="graphics/header.png" width="743" height="81" />

<?php if ($user): ?>
    <div class="noticeHere"></div>
    <img src="graphics/gamechoice.png" width="743" height="350" usemap="#gamechoice" />
    
    <map id="gamechoice" name="gamechoice"><area shape="rect" alt="free slap" title="" coords="451,79,641,160" href="" target="" /><area shape="rect" alt="" title="" coords="513,198,702,280" href="challenge.php" target="" /></map>
    
    <input type="button" id="pickcolour"
      onclick="sendRequestToManyRecipients(); return false;"
      value="&nbsp;"
      style="position: relative; top: -270px; left: 451px; height: 80px;
        background: transparent; border: 0px; width: 150px;"
    />
    
    <!-- <fb:add-to-timeline></fb:add-to-timeline> -->

    <div class="footer">
    <a href="<?php echo $logoutUrl; ?>"><img src="graphics/fb-logout.png"/></a>
    </div>
<?php elseif(!$user): ?>
    <u>This game is currently under development. Nothing to see here.</u>
    <br/><br/><br/>
    To play this game, you have to be logged into Facebook.<br/>
    <a href="<?php echo $loginUrl; ?>"><img src="graphics/fb-login.png"/></a>
<?php endif; ?>

</div>
</body>
</html>